package itacademy.com.project017;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class FirstFragment extends Fragment implements View.OnClickListener {

    private Button btnAdd;
    private EditText etName;

    private FragmentCallback mCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        btnAdd = view.findViewById(R.id.btnAdd);
        etName = view.findViewById(R.id.etName);
        btnAdd.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
        mCallback = (FragmentCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " need to implement FragmentCallback");
        }

    }

    @Override
    public void onClick(View view) {
        String name = etName.getText().toString();
        mCallback.addName(name);
    }
}
