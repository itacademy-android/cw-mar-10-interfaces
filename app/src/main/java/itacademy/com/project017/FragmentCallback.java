package itacademy.com.project017;

import java.util.ArrayList;

public interface FragmentCallback {

    void addName(String name);

    ArrayList<String> getNamesList();

}
